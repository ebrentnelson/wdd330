import LSStore from './ls.js';
import { createElement } from './utilities.js';

class ToDos {
  constructor(elementId, lsKey) {
    this.container = document.getElementById(elementId);
    this.store = new LSStore(lsKey);
    this.showing = 'all';
  }

  clear() {
    this.container.innerHTML = '';
  }

  updateShowing(val) {
    this.showing = val;
    this.render();
  }

  addTodo(task) {
    if (task) {
      const id = Date.now();
      const todo = { id, content: task, completed: false };
      this.store.set(id, todo);
      this.render({ focusInput: true });
    }
  }

  updateTodo(item) {
    this.store.set(item.id, item);
    this.render();
  }

  removeTodo(item) {
    this.store.remove(item.id);
    this.render();
  }

  createListItems() {
    return this.store
      .list()
      .filter((i) => {
        if (this.showing === 'all') {
          return true;
        } else if (this.showing === 'active' && !i.completed) {
          return true;
        } else if (this.showing === 'completed' && i.completed) {
          return true;
        } else {
          return false;
        }
      })
      .map((i) => {
        const checkbox = createElement('input', {
          type: 'checkbox',
          checked: i.completed ? 'checked' : undefined,
        });
        const task = createElement('span', { textContent: i.content });
        const label = createElement('label', {}, [checkbox, task]);
        const button = createElement('button', {
          innerHTML: '&#10060;',
          className: 'remove',
        });
        const li = createElement(
          'li',
          {
            className: i.completed ? 'completed' : 'active',
          },
          [label, button]
        );

        checkbox.addEventListener('click', (e) => {
          this.updateTodo({ ...i, completed: e.target.checked });
        });
        button.addEventListener('click', () => this.removeTodo(i));

        return li;
      });
  }

  createListFooter() {
    const tasksLeft = this.store.list().filter((i) => !i.completed).length;
    const remaining = createElement('span', {
      textContent: `${tasksLeft} tasks left`,
    });
    const allBtn = createElement('button', {
      textContent: 'All',
      className: this.showing === 'all' ? 'active' : '',
    });
    const activeBtn = createElement('button', {
      textContent: 'Active',
      className: this.showing === 'active' ? 'active' : '',
    });
    const completedBtn = createElement('button', {
      textContent: 'Completed',
      className: this.showing === 'completed' ? 'active' : '',
    });
    const li = createElement('li', { className: 'footer' }, [
      remaining,
      allBtn,
      activeBtn,
      completedBtn,
    ]);

    allBtn.addEventListener('click', () => this.updateShowing('all'));
    activeBtn.addEventListener('click', () => this.updateShowing('active'));
    completedBtn.addEventListener('click', () =>
      this.updateShowing('completed')
    );

    return li;
  }

  renderHeader() {
    const h1 = createElement('h1', { textContent: 'Todos' });
    const header = createElement('header', {}, [h1]);
    this.container.appendChild(header);
  }

  renderList() {
    const list = createElement('ul', { className: 'todo-list' }, [
      ...this.createListItems(),
      this.createListFooter(),
    ]);

    const main = createElement('main', {}, [list]);
    this.container.appendChild(main);
  }

  renderFooter({ focusInput }) {
    const input = createElement('input', {
      type: 'text',
      className: 'new-task',
      placeholder: 'Create a new task...',
      'aria-label': 'New task input',
    });
    const button = createElement('button', { innerHTML: '&#10133;' });

    button.addEventListener('click', () => this.addTodo(input.value));
    input.addEventListener('keyup', (e) => {
      if (e.keyCode === 13) {
        this.addTodo(input.value);
      }
    });

    const footer = createElement('footer', {}, [input, button]);
    this.container.appendChild(footer);

    if (focusInput) {
      input.focus();
    }
  }

  render(opts = {}) {
    this.clear();
    this.renderHeader(opts);
    this.renderList(opts);
    this.renderFooter(opts);
  }
}

export default ToDos;
