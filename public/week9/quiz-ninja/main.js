const quizEndpoint =
  'https://raw.githubusercontent.com/spbooks/jsninja2/master/Ch%2013/quiz/questions.json';

const view = {
  score: document.querySelector('#score strong'),
  question: document.getElementById('question'),
  result: document.getElementById('result'),
  info: document.getElementById('info'),
  start: document.getElementById('start'),
  response: document.querySelector('#response'),
  timer: document.querySelector('#timer strong'),
  hiScore: document.querySelector('#hiScore strong'),
  buttons(array) {
    return array.map((value) => `<button>${value}</button>`).join('');
  },
  render(target, content, attributes) {
    for (const key in attributes) {
      target.setAttribute(key, attributes[key]);
    }
    target.innerHTML = content;
  },
  show(element) {
    element.style.display = 'block';
  },
  hide(element) {
    element.style.display = 'none';
  },
  setup() {
    this.show(this.question);
    this.show(this.response);
    this.show(this.result);
    this.hide(this.start);
    this.render(this.score, game.score);
    this.render(this.result, '');
    this.render(this.info, '');
    this.render(this.hiScore, game.hiScore());
  },
  cleanup() {
    this.hide(this.question);
    this.hide(this.response);
    this.show(this.start);
  },
};

function shuffle(arr) {
  arr.sort(() => (Math.random() > 0.5 ? 1 : -1));
}

const game = {
  start(quiz) {
    console.log('start() invoked');
    this.score = 0;
    this.secondsRemaining = 20;
    this.timer = setInterval(this.countdown.bind(this), 1000);
    this.questions = [...quiz];
    view.setup();
    this.ask();
  },
  ask() {
    console.log('ask() invoked');
    if (this.questions.length > 2) {
      shuffle(this.questions);
      this.question = this.questions.pop();
      const options = [
        this.questions[0].realName,
        this.questions[1].realName,
        this.question.realName,
      ];
      shuffle(options);
      const question = `What is ${this.question.name}'s real name?`;
      view.render(view.question, question);
      view.render(view.response, view.buttons(options));
    } else {
      this.end();
    }
  },
  check(event) {
    console.log('check(event) invoked');
    const response = event.target.textContent;
    const answer = this.question.realName;
    if (response === answer) {
      view.render(view.result, 'Correct!', { class: 'correct' });
      this.score++;
      view.render(view.score, this.score);
    } else {
      view.render(view.result, `Wrong! The correct answer was ${answer}`, {
        class: 'wrong',
      });
    }
    this.ask();
  },
  countdown() {
    this.secondsRemaining--;
    view.render(view.timer, this.secondsRemaining);
    if (this.secondsRemaining < 0) {
      this.end();
    }
  },
  hiScore() {
    const hi = localStorage.getItem('highScore') || 0;
    if (this.score > hi || hi === 0) {
      localStorage.setItem('highScore', this.score);
      view.render(view.info, '** NEW HIGH SCORE! **');
    }
    return localStorage.getItem('highScore');
  },
  end() {
    console.log('end() invoked');
    view.render(
      view.info,
      `Game Over, you scored ${this.score} point${this.score !== 1 ? 's' : ''}`
    );
    view.cleanup();
    clearInterval(this.timer);
    this.render(this.hiScore, this.hiScore());
  },
};

view.start.addEventListener(
  'click',
  async () => {
    const result = await fetch(quizEndpoint);
    const quiz = await result.json();
    game.start(quiz.questions);
  },
  false
);
view.response.addEventListener('click', (event) => game.check(event), false);
