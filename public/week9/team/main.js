const dict = document.querySelectorAll('div[data-key]'); // any div with data-key
console.log(dict);

document.addEventListener('keyup', (event) => {
  const codes = [...dict].map((item) => parseInt(item.dataset.key));
  console.log(codes);
  if (codes.includes(event.code)) {
    let audio = document.querySelector(`audio[data-key='${event.code}']`);
    audio.currentTime = 0;
    audio.play(); // video also has play method
    let label = document.querySelector(`div[data-key='${event.code}']`);
    label.setAttribute('class', 'key playing');
    audio.addEventListener('ended', () => {
      label.setAttribute('class', 'key');
    });
    const style = window.getComputedStyle(label);
    const matrix = style['transform'];
    let matrixValue = parseInt(
      matrix.match(/matrix.*\((.+)\)/)[1].split(', ')[5]
    );
    if (matrixValue <= 100) {
      matrixValue += 10;
    } else {
      matrixValue = 0;
    }
    label.style.transform = `translateY(${matrixValue}px)`;
  }
});
