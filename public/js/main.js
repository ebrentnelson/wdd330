async function init() {
  route(); // routes to toc.json by default

  document.querySelector('.copyright-year').innerHTML =
    new Date().getFullYear();
  document.querySelector('.last-updated > .date').innerHTML =
    document.lastModified;
}

async function populateContents(dataUri) {
  const data = await (await fetch(`${dataUri}?_=${Math.random()}`)).json();

  const tableOfContents = document.querySelector('.table-of-contents');

  if (tableOfContents) {
    tableOfContents.innerHTML = ''; // clear out preexisting children

    document.querySelector('.contents-title').textContent = data.title;

    data.contents.forEach((c) => {
      const listItem = createListItem(c);
      tableOfContents.appendChild(listItem);
    });
  }
}

function createListItem(link) {
  const li = document.createElement('li');

  const a = document.createElement('a');
  a.setAttribute('href', link.url);
  a.innerHTML = link.label;
  if (link.target) {
    a.setAttribute('target', link.target);
  }

  li.appendChild(a);

  return li;
}

function route() {
  const hash = location.hash.substring(1) || 'toc'; // strip off #, default to toc

  if (hash) {
    populateContents(`data/${hash}.json`);
  }
}

window.addEventListener('load', init);
window.addEventListener('hashchange', route);
