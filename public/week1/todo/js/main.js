const KEY = 'my-todos';

function init() {
  document.querySelector('.add-todo').addEventListener('click', addTodo);
  renderTodos();
}

function addTodo() {
  const newTodo = document.querySelector('.todo').value;

  if (!newTodo) {
    return;
  }

  const list = loadTodos(KEY);
  list.add(newTodo);
  saveTodos(KEY, list);

  renderTodos();

  document.querySelector('.todo').value = '';
}

function removeTodo(todo) {
  const list = loadTodos(KEY);
  list.delete(todo);
  saveTodos(KEY, list);

  renderTodos();
}

function renderTodos() {
  const listContainer = document.querySelector('.todo-list');

  // clean up list in preparation for re-render
  while (listContainer.firstChild) {
    listContainer.removeChild(listContainer.firstChild);
  }

  const list = loadTodos(KEY);
  list.forEach((li) => {
    listContainer.appendChild(todoItemRenderer(li));
  });
}

function todoItemRenderer(item) {
  const li = document.createElement('li');

  const checkbox = document.createElement('input');
  checkbox.setAttribute('type', 'checkbox');
  checkbox.addEventListener('click', removeTodo.bind(null, item));
  li.appendChild(checkbox);

  const span = document.createElement('span');
  span.textContent = item;
  li.appendChild(span);

  return li;
}

function saveTodos(key, set) {
  localStorage.setItem(key, JSON.stringify([...set]));
}

function loadTodos(key) {
  return new Set(JSON.parse(localStorage.getItem(key)));
}

window.addEventListener('load', init);
