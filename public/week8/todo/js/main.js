// main.js
import ToDos from './ToDos.js';

function initialize() {
  const todos = new ToDos('todo-list', 'todo');
  todos.render();
}

window.addEventListener('load', initialize);
