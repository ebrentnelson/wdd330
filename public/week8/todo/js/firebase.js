// Import the functions you need from the SDKs you need
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.2.0/firebase-app.js';
import { getDatabase } from 'https://www.gstatic.com/firebasejs/9.2.0/firebase-database.js';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyC89kuhlo7oX3uawGqoU3u-g46wqwJWSmo',
  authDomain: 'wdd330-dc8ac.firebaseapp.com',
  projectId: 'wdd330-dc8ac',
  storageBucket: 'wdd330-dc8ac.appspot.com',
  messagingSenderId: '622712033282',
  appId: '1:622712033282:web:34c5fb32d1c0690e18d71b',
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const db = getDatabase(app);
