import {
  ref,
  set,
  get,
  child,
} from 'https://www.gstatic.com/firebasejs/9.2.0/firebase-database.js';

import { db } from './firebase.js';

const DELIM = '/';

class FBStore {
  constructor(key) {
    this.storeKey = key;
    this.cachedList = null;
  }

  // lazily load list
  async list() {
    if (!this.cachedList) {
      const dbRef = ref(db);
      const remoteList = await get(child(dbRef, `${this.storeKey}${DELIM}`));
      if (remoteList.exists()) {
        this.cachedList = Object.values(remoteList.val());
      } else {
        this.cachedList = [];
      }
    }

    // order by time desc
    this.cachedList.sort((a, b) => b.timestamp - a.timestamp);
    return this.cachedList;
  }

  // optimisic update
  set(key, obj) {
    if (obj) {
      if (!this.cachedList.find((item) => item.id === key)) {
        this.cachedList.push(obj);
      } else {
        this.cachedList = this.cachedList.map((item) => {
          if (item.id === key) {
            return obj;
          }
          return item;
        });
      }
    }
    try {
      set(ref(db, `${this.storeKey}${DELIM}${key}`), obj);
    } catch (e) {
      console.error(e);
    }
  }

  // pull from cached list
  get(key) {
    return this.cachedList.find((item) => item.id === key);
  }

  // optimisic update
  remove(key) {
    const obj = this.cachedList.find((item) => item.id === key);
    if (obj) {
      this.cachedList.splice(this.cachedList.indexOf(obj), 1);
    }
    try {
      this.set(key, null);
    } catch (e) {
      console.error(e);
    }
  }
}

export default FBStore;
