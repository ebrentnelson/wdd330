import FBStore from './FBStore.js';
import { createElement } from './utilities.js';

class ToDos {
  constructor(elementId, lsKey) {
    this.container = document.getElementById(elementId);
    this.store = new FBStore(lsKey);
    this.showing = 'all';
    this.draggingId = null;
    this.draggedOverId = null;
  }

  clear() {
    this.container.innerHTML = '';
  }

  updateShowing(val) {
    this.showing = val;
    this.render();
  }

  addTodo(task) {
    if (task) {
      const id = Date.now();
      const todo = { id, timestamp: id, content: task, completed: false };
      this.store.set(id, todo);
      this.render({ focusInput: true });
    }
  }

  async updateTodo(item) {
    this.store.set(item.id, item);
    await this.render();
  }

  removeTodo(item) {
    this.store.remove(item.id);
    this.render();
  }

  setDraggingId = (e) => {
    this.draggingId = parseInt(e.currentTarget.dataset.id);
  };

  setDraggedOverId = (e) => {
    e.preventDefault();
    this.draggedOverId = parseInt(e.currentTarget.dataset.id);
  };

  reorderItems = async () => {
    if (this.draggingId && this.draggedOverId) {
      const draggedItem = this.store.get(this.draggingId);
      const draggedOverItem = this.store.get(this.draggedOverId);

      draggedItem.timestamp = draggedOverItem.timestamp - 1;
      draggedOverItem.timestamp = draggedOverItem.timestamp + 1;

      await this.updateTodo(draggedItem);
      await this.updateTodo(draggedOverItem);

      this.draggingId = null;
      this.draggedOverId = null;
    }
  };

  async createListItems() {
    return (await this.store.list())
      .filter((i) => {
        if (this.showing === 'all') {
          return true;
        } else if (this.showing === 'active' && !i.completed) {
          return true;
        } else if (this.showing === 'completed' && i.completed) {
          return true;
        } else {
          return false;
        }
      })
      .map((i) => {
        const checkbox = createElement('input', {
          type: 'checkbox',
          checked: i.completed ? 'checked' : undefined,
        });
        const task = createElement('span', { textContent: i.content });
        const label = createElement('label', {}, [checkbox, task]);
        const button = createElement('button', {
          innerHTML: '&#10060;',
          className: 'remove',
        });
        const li = createElement(
          'li',
          {
            ['data-id']: i.id,
            className: i.completed ? 'completed' : 'active',
            draggable: true,
          },
          [label, button]
        );

        li.addEventListener('drag', this.setDraggingId);
        li.addEventListener('dragover', this.setDraggedOverId);
        li.addEventListener('drop', this.reorderItems);

        checkbox.addEventListener('click', (e) => {
          this.updateTodo({ ...i, completed: e.target.checked });
        });
        button.addEventListener('click', () => this.removeTodo(i));

        return li;
      });
  }

  async createListFooter() {
    const tasksLeft = (await this.store.list()).filter(
      (i) => !i.completed
    ).length;
    const remaining = createElement('span', {
      textContent: `${tasksLeft} tasks left`,
    });
    const allBtn = createElement('button', {
      textContent: 'All',
      className: this.showing === 'all' ? 'active' : '',
    });
    const activeBtn = createElement('button', {
      textContent: 'Active',
      className: this.showing === 'active' ? 'active' : '',
    });
    const completedBtn = createElement('button', {
      textContent: 'Completed',
      className: this.showing === 'completed' ? 'active' : '',
    });
    const li = createElement('li', { className: 'footer' }, [
      remaining,
      allBtn,
      activeBtn,
      completedBtn,
    ]);

    allBtn.addEventListener('click', () => this.updateShowing('all'));
    activeBtn.addEventListener('click', () => this.updateShowing('active'));
    completedBtn.addEventListener('click', () =>
      this.updateShowing('completed')
    );

    return li;
  }

  renderHeader() {
    const h1 = createElement('h1', { textContent: 'Todos' });
    const header = createElement('header', {}, [h1]);
    this.container.appendChild(header);
  }

  async renderList() {
    const list = createElement('ul', { className: 'todo-list' }, [
      ...(await this.createListItems()),
      await this.createListFooter(),
    ]);

    const main = createElement('main', {}, [list]);
    this.container.appendChild(main);
  }

  renderFooter({ focusInput }) {
    const input = createElement('input', {
      type: 'text',
      className: 'new-task',
      placeholder: 'Create a new task...',
      'aria-label': 'New task input',
    });
    const button = createElement('button', { innerHTML: '&#10133;' });

    button.addEventListener('click', () => this.addTodo(input.value));
    input.addEventListener('keyup', (e) => {
      if (e.keyCode === 13) {
        this.addTodo(input.value);
      }
    });

    const footer = createElement('footer', {}, [input, button]);
    this.container.appendChild(footer);

    if (focusInput) {
      input.focus();
    }
  }

  async render(opts = {}) {
    this.clear();
    this.renderHeader(opts);
    await this.renderList(opts);
    this.renderFooter(opts);
  }
}

export default ToDos;
