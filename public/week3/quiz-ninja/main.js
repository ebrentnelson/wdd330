const quiz = [
  { name: 'Superman', realName: 'Clark Kent' },
  { name: 'Wonder Woman', realName: 'Diana Prince' },
  { name: 'Batman', realName: 'Bruce Wayne' },
];

const view = {
  score: document.querySelector('#score strong'),
  question: document.getElementById('question'),
  result: document.getElementById('result'),
  info: document.getElementById('info'),
  start: document.getElementById('start'),
  render(target, content, attributes) {
    for (const key in attributes) {
      target.setAttribute(key, attributes[key]);
    }
    target.innerHTML = content;
  },
  show(element) {
    element.style.display = 'block';
  },
  hide(element) {
    element.style.display = 'none';
  },
};

const game = {
  start(quiz) {
    view.hide(view.start);
    this.score = 0;
    for (const { name, realName } of quiz) {
      const response = this.ask(name);
      this.check(response, realName);
    }
    this.end();
  },
  ask(name) {
    const question = `What is ${name}'s real name?`;
    view.render(view.question, question);
    return prompt(question);
  },
  check(response, answer) {
    if (response === answer) {
      view.render(view.result, 'Correct!', { class: 'correct' });
      alert('Correct');
      this.score++;
      view.render(view.score, this.score);
    } else {
      view.render(view.result, `Wrong! The correct answer was ${answer}`, {
        class: 'wrong',
      });
      alert(`Wrong! The correct answer was ${answer}`);
    }
  },
  end() {
    view.show(view.start);
    view.render(
      view.info,
      `Game Over, you scored ${this.score} point${this.score !== 1 ? 's' : ''}`
    );
  },
};

view.start.addEventListener('click', () => game.start(quiz), false);
