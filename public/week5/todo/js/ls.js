const DELIM = '::';

class LSStore {
  constructor(key) {
    this.storeKey = key;
  }

  list() {
    const list = [];
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      if (key.indexOf(`${this.storeKey}${DELIM}`) === 0) {
        // relevant item to current store
        const itemKey = key.replace(`${this.storeKey}${DELIM}`, '');
        list.push(this.get(itemKey));
      }
    }
    // order by time desc
    list.sort((a, b) => b.id - a.id);
    return list;
  }

  set(key, obj) {
    localStorage.setItem(`${this.storeKey}${DELIM}${key}`, JSON.stringify(obj));
  }

  get(key) {
    return JSON.parse(localStorage.getItem(`${this.storeKey}${DELIM}${key}`));
  }

  remove(key) {
    localStorage.removeItem(`${this.storeKey}${DELIM}${key}`);
  }
}

export default LSStore;
